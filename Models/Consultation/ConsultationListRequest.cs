namespace mvc.Models.Consultation;

public class ConsultationListRequest
{
    public string? Patient { get; set; }
    public string? Specialist { get; set; }
    public string? Doctor { get; set; }
}