namespace mvc.Models.Consultation;

public class ConsultationListResponse
{
    public int Id { get; set; }
    public string? Doctor { get; set; }
    public string? Specialist { get; set; }
    public string? Patient { get; set; }
}