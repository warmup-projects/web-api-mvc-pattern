namespace mvc.Models.Consultation;

public class ConsultationDecodeModel
{
    public string? Patient { get; set; }
    public string? Doctor { get; set; }
}