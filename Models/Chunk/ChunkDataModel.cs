namespace mvc.Models.Chunk;

public class ChunkDataModel
{
    public string chunk { get; set; }
    public string column { get; set; }
    public string table { get; set; }
}