namespace mvc.Models;

public class BaseResponse<T>
{
    public string Code { get; set; } = "00";
    public string Message { get; set; } = "Success";
    public T Data { get; set; }
}