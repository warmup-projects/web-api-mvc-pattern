using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("logs")]
public class LogDbo
{
    public LogDbo(string level, string message)
    {
        Level = level;
        Message = message;
    }

    public int Id { get; set; }
    public string Level { get; set; }
    public string Message { get; set; }
    public DateTime Timestamp { get; set; } = DateTime.Now;

    public static void OnModelCreating(EntityTypeBuilder<LogDbo> entity)
    {
    }
}