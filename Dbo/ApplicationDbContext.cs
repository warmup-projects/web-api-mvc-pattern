using Microsoft.EntityFrameworkCore;

namespace mvc.Dbo;

public class ApplicationDbContext : DbContext
{
    protected ApplicationDbContext()
    {
    }

    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
    }
    
    public DbSet<LogDbo> Logs { get; set; } = null!;
    public DbSet<DoctorDbo> Doctors { get; set; } = null!;
    public DbSet<DoctorNewDbo> DoctorsNew { get; set; } = null!;
    public DbSet<PatientDbo> Patients { get; set; } = null!;
    public DbSet<PatientDiseaseDbo> PatientDiseases { get; set; } = null!;
    public DbSet<ConsultationDbo> Consultations { get; set; } = null!;
    public DbSet<ChunkHashedDbo> ChunkHashed { get; set; } = null!;
    public DbSet<ChunkDataDbo> ChunkData { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<LogDbo>(LogDbo.OnModelCreating);
        modelBuilder.Entity<DoctorDbo>(DoctorDbo.OnModelCreating);
        modelBuilder.Entity<DoctorNewDbo>(DoctorNewDbo.OnModelCreating);
        modelBuilder.Entity<PatientDbo>(PatientDbo.OnModelCreating);
        modelBuilder.Entity<PatientDiseaseDbo>(PatientDiseaseDbo.OnModelCreating);
        modelBuilder.Entity<ConsultationDbo>(ConsultationDbo.OnModelCreating);
        modelBuilder.Entity<ChunkHashedDbo>(ChunkHashedDbo.OnModelCreating);
        modelBuilder.Entity<ChunkDataDbo>(ChunkDataDbo.OnModelCreating);
        
        foreach (var entity in modelBuilder.Model.GetEntityTypes())
        {
            // Convert column names to snake_case
            foreach (var property in entity.GetProperties())
            {
                property.SetColumnName(ConvertToSnakeCase(property.GetColumnName()));
            }

            // Convert key names to snake_case
            foreach (var key in entity.GetKeys())
            {
                key.SetName(ConvertToSnakeCase(key.GetName() ?? ""));
            }

            // Convert foreign key names to snake_case
            foreach (var foreignKey in entity.GetForeignKeys())
            {
                foreignKey.SetConstraintName(ConvertToSnakeCase(foreignKey.GetConstraintName() ?? ""));
            }
        }
    }

    private string ConvertToSnakeCase(string input)
    {
        return string.Concat(input.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
    }
}