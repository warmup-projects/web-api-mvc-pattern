using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("patient_diseases")]
public class PatientDiseaseDbo
{
    public int Id { get; set; }
    public int PatientId { get; set; }
    public string Name { get; set; }
    public bool HasRecovered  { get; set; }
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; }
    [ForeignKey("PatientId")]
    public PatientDbo? Patient { get; set; }

    public static void OnModelCreating(EntityTypeBuilder<PatientDiseaseDbo> entity)
    {
    }
}