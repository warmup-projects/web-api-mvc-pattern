using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("chunk_data")]
public class ChunkDataDbo
{
    public int Id { get; set; }
    public string ChunkHashedId { get; set; }
    public string TableRowId { get; set; }
    public string TableName { get; set; }
    public string? TableColumnName { get; set; }
    
    public static void OnModelCreating(EntityTypeBuilder<ChunkDataDbo> entity)
    {
    }
}