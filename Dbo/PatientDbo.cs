using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("patients")]
public class PatientDbo
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<PatientDiseaseDbo> PatientDiseases { get; set; } = new();
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; }
    
    public static void OnModelCreating(EntityTypeBuilder<PatientDbo> entity)
    {
    }
}