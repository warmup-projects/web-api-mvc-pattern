using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("consultations")]
public class ConsultationDbo
{
    public int Id { get; set; }
    public int PatientId { get; set; }
    public int DoctorId { get; set; }
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; }
    [ForeignKey("PatientId")]
    public PatientDbo? Patient { get; set; }
    [ForeignKey("DoctorId")]
    public DoctorDbo? Doctor { get; set; }
    
    public static void OnModelCreating(EntityTypeBuilder<ConsultationDbo> entity)
    {
    }
}