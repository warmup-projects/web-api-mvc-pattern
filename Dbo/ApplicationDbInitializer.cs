using mvc.Common;

namespace mvc.Dbo;

public static class ApplicationDbInitializer
{
    public static void Initialize(ApplicationDbContext context, bool isWithHashing = false)
    {
        if (context.Doctors.Any()) return;

        var doctors = new DoctorDbo[10000];
        for (int i = 0; i < 10000; i+=2)
        {
            doctors[i] = new DoctorDbo { Name = "Mohammed", Specialist = "Dentist", CreatedAt = DateTime.Now };
            doctors[i + 1] = new DoctorDbo
                { Name = "Farrell", Specialist = "Internal Organ", CreatedAt = DateTime.Now };
        }
        // doctors = new DoctorDbo[]
        // {
        //     new() { Name = "Mohammed", Specialist = "Dentist", CreatedAt = DateTime.Now },
        //     new() { Name = "Farrell", Specialist = "Internal Organ", CreatedAt = DateTime.Now }
        // };

        var patients = new PatientDbo[]
        {
            new() { Name = "Eghar", CreatedAt = DateTime.Now },
            new() { Name = "Syahputra", CreatedAt = DateTime.Now }
        };

        var patientDiseases = new PatientDiseaseDbo[]
        {
            new() { Name = "Cavity", HasRecovered = false, PatientId = 1, CreatedAt = DateTime.Now },
            new() { Name = "Heart Attack", HasRecovered = false, PatientId = 1, CreatedAt = DateTime.Now },
            new() { Name = "Stroke", HasRecovered = false, PatientId = 2, CreatedAt = DateTime.Now },
            new() { Name = "Swollen Teeth", HasRecovered = false, PatientId = 2, CreatedAt = DateTime.Now }
        };

        var consultations = new ConsultationDbo[]
        {
            new() { PatientId = 1, DoctorId = 1, CreatedAt = DateTime.Now },
            new() { PatientId = 1, DoctorId = 2, CreatedAt = DateTime.Now },
            new() { PatientId = 2, DoctorId = 1, CreatedAt = DateTime.Now },
            new() { PatientId = 2, DoctorId = 2, CreatedAt = DateTime.Now }
        };

        if (isWithHashing)
            ExecuteHashing(context, doctors, patients);
        
        context.Doctors.AddRange(doctors);
        context.SaveChanges();
        
        context.Patients.AddRange(patients);
        context.SaveChanges();
        
        context.PatientDiseases.AddRange(patientDiseases);
        context.SaveChanges();
        
        context.Consultations.AddRange(consultations);
        context.SaveChanges();
    }

    private static void ExecuteHashing(ApplicationDbContext context, DoctorDbo[] doctors, PatientDbo[] patients)
    {
        List<ChunkHashedDbo> chunkHashedList = new List<ChunkHashedDbo>();
        List<ChunkDataDbo> chunkDataList = new List<ChunkDataDbo>();

        int index = 1;
        foreach (var doctor in doctors)
        {
            List<string> chunks = doctor.Name.Breakdown(3);
            doctor.Name = doctor.Name.Base64Encode();
            doctor.Specialist = doctor.Specialist.Base64Encode();
            chunks.ForEach(chunk =>
            {
                chunkHashedList.Add(new ChunkHashedDbo { Chunk = chunk });
                chunkDataList.Add(new ChunkDataDbo
                {
                    ChunkHashedId = chunk,
                    TableRowId = index.ToString(),
                    TableName = Constants.DoctorsTable
                });
            });
            index++;
        }

        index = 1;
        foreach (var patient in patients)
        {
            List<string> chunks = patient.Name.Breakdown(3);
            patient.Name = patient.Name.Base64Encode();
            chunks.ForEach(chunk =>
            {
                chunkHashedList.Add(new ChunkHashedDbo { Chunk = chunk });
                chunkDataList.Add(new ChunkDataDbo
                {
                    ChunkHashedId = chunk,
                    TableRowId = index.ToString(),
                    TableName = Constants.PatientsTable
                });
            });
            index++;
        }
        
        chunkHashedList = chunkHashedList.DistinctBy(asd => asd.Chunk).ToList();
        chunkDataList = chunkDataList.Select(chunkData =>
        {
            chunkData.ChunkHashedId = (chunkHashedList
                .FindIndex(chunkHashed => chunkHashed.Chunk.Equals(chunkData.ChunkHashedId)) + 1)
                .ToString();
            return chunkData;
        }).ToList();
        
        chunkHashedList.ForEach(chunkHashed =>
        {
            chunkHashed.Chunk = chunkHashed.Chunk.ComputeSha256Hash();
        });
        
        context.ChunkHashed.AddRange(chunkHashedList);
        context.SaveChanges();

        context.ChunkData.AddRange(chunkDataList);
        context.SaveChanges();
    }
}