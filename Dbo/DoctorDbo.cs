using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("doctors")]
public class DoctorDbo
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Specialist { get; set; }
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; }
    
    public static void OnModelCreating(EntityTypeBuilder<DoctorDbo> entity)
    {
    }
}