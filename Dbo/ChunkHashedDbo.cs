using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("chunk_hashed")]
public class ChunkHashedDbo
{
    public int Id { get; set; }
    public string Chunk { get; set; } = null!;
    
    public static void OnModelCreating(EntityTypeBuilder<ChunkHashedDbo> entity)
    {
    }
}