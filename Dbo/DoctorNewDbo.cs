using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace mvc.Dbo;

[Table("doctors_new")]
public class DoctorNewDbo
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Specialist { get; set; }
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    public DateTime? UpdatedAt { get; set; }
    
    public static void OnModelCreating(EntityTypeBuilder<DoctorNewDbo> entity)
    {
    }
}