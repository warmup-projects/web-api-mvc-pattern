namespace mvc.Common;

public static class Constants
{
    public const string DoctorsTable = "doctors";
    public const string PatientsTable = "patients";
}