using System.Security.Cryptography;
using System.Text;

namespace mvc.Common;

public static class StringExtension
{
    public static List<string> Breakdown(this string input, int size)
    {
        List<string> chunkList = new List<string>();
        for (int i = 0; i <= input.Length - size; i++)
        {
            chunkList.Add(input.Substring(i, size));
        }

        return chunkList;
    }
    
    public static string Base64Encode(this string plainText)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        return Convert.ToBase64String(plainTextBytes);
    }
    
    public static string Base64Decode(this string base64EncodedData) 
    {
        var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return Encoding.UTF8.GetString(base64EncodedBytes);
    }
    
    public static string ComputeSha256Hash(this string input)
    {
        using SHA256 sha256 = SHA256.Create();
        // Convert the input string to a byte array and compute the hash
        byte[] hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Convert the byte array to a hexadecimal string representation
        StringBuilder stringBuilder = new StringBuilder();
        foreach (byte b in hashBytes)
        {
            stringBuilder.Append(b.ToString("x2"));
        }

        return stringBuilder.ToString();
    }
}