using FluentScheduler;
using Microsoft.EntityFrameworkCore;
using mvc.Dbo;
using mvc.Services.FluentScheduler;
using mvc.Services.Sample.Interface;
using mvc.Services.Transaction;
using mvc.Services.Transaction.Interface;

namespace mvc.Services;

public static class ServiceInitialization
{
    public static void ServiceInit(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("primary")));

        // Configure logging to database
        services.AddLogging(builder =>
        {
            builder.AddConsole();
            builder.AddDebug(); // AddDebug is optional, it logs to the debugger output window
        });

        services.AddControllers();
    }

    public static void DependencyInjection(this IServiceCollection services)
    {
        services.AddScoped<IConsultationService, ConsultationService>();
    }

    public static void ApplicationInit(this WebApplication app, IConfiguration configuration)
    {
        using (var scope = app.Services.CreateScope())
        {
            var services = scope.ServiceProvider;

            var context = services.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();
            ApplicationDbInitializer.Initialize(context, true);
        }

        IServiceScopeFactory serviceScopeFactory = app.Services.GetRequiredService<IServiceScopeFactory>();
        JobManager.Initialize(new FluentSchedulerJobService(serviceScopeFactory));
    }
}