using mvc.Models;
using mvc.Models.Consultation;

namespace mvc.Services.Transaction.Interface;

public interface IConsultationService
{
    public BaseResponse<List<ConsultationListResponse>> GetConsultationList(ConsultationListRequest request);
    public BaseResponse<ConsultationDecodeModel> ConsultationDecode(ConsultationListRequest request);
}