using Microsoft.EntityFrameworkCore;
using mvc.Common;
using mvc.Dbo;
using mvc.Models;
using mvc.Models.Chunk;
using mvc.Models.Consultation;
using mvc.Services.Transaction.Interface;

namespace mvc.Services.Transaction;

public class ConsultationService : IConsultationService
{
    private readonly ApplicationDbContext _context;
    
    public ConsultationService(ApplicationDbContext context)
    {
        _context = context;
    }

    public BaseResponse<List<ConsultationListResponse>> GetConsultationList(ConsultationListRequest request)
    {
        try
        {
            /* Using IQueryable */
            #region Manual Query
            // var consultationList =
            // (
            //     from consultation in _context.Consultations
            //     join doctor in _context.Doctors on consultation.DoctorId equals doctor.Id
            //     join patient in _context.Patients on consultation.PatientId equals patient.Id
            //     where doctor.Name.Contains(request.DoctorName) && patient.Name.Contains(request.PatientName)
            //     select new ConsultationListResponse
            //     {
            //         Doctor = doctor.Name,
            //         Patient = patient.Name
            //     }
            // ).AsNoTracking().ToList();
            #endregion

            #region Select Projection
            // var consultationList = _context.Consultations
            //     .Where(consultation => consultation.Doctor!.Name.Contains(request.Doctor ?? ""))
            //     .Where(consultation => consultation.Patient!.Name.Contains(request.Patient ?? ""))
            //     .Select(consultation => 
            //         new ConsultationListResponse()
            //         {
            //             Id = consultation.Id,
            //             Doctor = consultation.Doctor!.Name,
            //             Patient = consultation.Patient!.Name
            //         }
            //     ).AsNoTracking().ToList();
            #endregion

            // Doctor
            List<ChunkDataModel> chunkDataModels = new List<ChunkDataModel>();
            List<ConsultationListResponse> consultationList = new List<ConsultationListResponse>();
            List<string>? chunkDatas = request.Doctor?.Breakdown(3);
            List<string>? chunkHasheds = chunkDatas?.Select(chunkData => chunkData.ComputeSha256Hash()).ToList();

            if (!string.IsNullOrWhiteSpace(request.Doctor))
                chunkDataModels.AddRange(request.Doctor
                    .Breakdown(3)
                    .Select(data => new ChunkDataModel
                    {
                        chunk = data,
                        column = "name",
                        table = "doctors"
                    }).ToList());

            if (!string.IsNullOrWhiteSpace(request.Specialist))
                chunkDataModels.AddRange(request.Specialist
                    .Breakdown(3)
                    .Select(data => new ChunkDataModel
                    {
                        chunk = data,
                        column = "specialist",
                        table = "doctors"
                    }).ToList());
            
            if (!string.IsNullOrWhiteSpace(request.Patient))
                chunkDataModels.AddRange(request.Patient
                    .Breakdown(3)
                    .Select(data => new ChunkDataModel
                    {
                        chunk = data,
                        column = "name",
                        table = "patients"
                    }).ToList());

            if (chunkHasheds?.Count > 0)
            {
                List<ChunkDataDbo> chunkDataList = (
                    from chunkData in _context.ChunkData
                    join chunkHashed in _context.ChunkHashed on chunkData.ChunkHashedId equals chunkHashed.Id.ToString()
                    where chunkHasheds.Contains(chunkHashed.Chunk)
                    group chunkData by chunkData.TableRowId into grouped
                    select new ChunkDataDbo
                    {
                        Id = grouped.Min(cd => cd.Id),
                        ChunkHashedId = grouped.Key,
                        TableRowId = grouped.Key,
                        TableName = grouped.First().TableName
                    }
                ).AsNoTracking().ToList();
                
                List<ChunkDataDbo> asd = (
                    from chunkData in _context.ChunkData
                    join chunkHashed in _context.ChunkHashed on chunkData.ChunkHashedId equals chunkHashed.Id.ToString()
                    where chunkHasheds.Contains(chunkHashed.Chunk)
                    group chunkData by new { chunkData.TableRowId, chunkData.TableName, chunkData.TableColumnName } into grouped
                    select new ChunkDataDbo
                    {
                        Id = grouped.Min(cd => cd.Id),
                        TableRowId = grouped.Key.TableRowId,
                        TableName = grouped.Key.TableName,
                        TableColumnName = grouped.Key.TableColumnName
                    }
                ).AsNoTracking().ToList();
                
                

                if (chunkDataList.Count > 0)
                {
                    consultationList =
                    (
                        from consultation in _context.Consultations
                        join doctor in _context.Doctors on consultation.DoctorId equals doctor.Id
                        join patient in _context.Patients on consultation.PatientId equals patient.Id
                        join chunkData in _context.ChunkData on consultation.DoctorId.ToString() equals chunkData.TableRowId
                        where
                            chunkDataList.Contains(chunkData) &&
                            chunkDataList.Select(cd => cd.TableRowId).Contains(consultation.DoctorId.ToString())
                        select new ConsultationListResponse
                        {
                            Doctor = doctor.Name,
                            Specialist = doctor.Specialist,
                            Patient = patient.Name
                        }
                    ).AsNoTracking().ToList();
                }
            }
            else
            {
                consultationList =
                (
                    from consultation in _context.Consultations
                    join doctor in _context.Doctors on consultation.DoctorId equals doctor.Id
                    join patient in _context.Patients on consultation.PatientId equals patient.Id
                    select new ConsultationListResponse
                    {
                        Doctor = doctor.Name,
                        Specialist = doctor.Specialist,
                        Patient = patient.Name
                    }
                ).AsNoTracking().ToList();
            }
            
            return new BaseResponse<List<ConsultationListResponse>>()
            {
                Data = consultationList
            };
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public BaseResponse<ConsultationDecodeModel> ConsultationDecode(ConsultationListRequest request)
    {
        return new BaseResponse<ConsultationDecodeModel>()
        {
            Data = new ConsultationDecodeModel()
            {
                Doctor = request.Doctor?.Base64Decode(),
                Patient = request.Patient?.Base64Decode()
            }
        };
    }
}