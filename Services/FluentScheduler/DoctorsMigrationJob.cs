using FluentScheduler;
using mvc.Dbo;

namespace mvc.Services.FluentScheduler;

public class DoctorsMigrationJob : IJob
{
    private IServiceScopeFactory _service;

    public DoctorsMigrationJob(IServiceScopeFactory service)
    {
        _service = service;
    }

    public void Execute()
    {
        try
        {
            MigrateData(1);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private void MigrateData(int id)
    {
        using var serviceScope = _service.CreateScope();
        var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

        try
        {
            var data = context.Doctors.Find(id);

            if (data != null)
            {
                id += 1;
                context.DoctorsNew.Add(new DoctorNewDbo
                {
                    Name = data.Name,
                    Specialist = data.Specialist,
                    CreatedAt = DateTime.Now
                });
                context.SaveChanges();
                MigrateData(id);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}