using FluentScheduler;
using mvc.Dbo;

namespace mvc.Services.FluentScheduler;

public class FluentSchedulerJobService : Registry
{
    public FluentSchedulerJobService(IServiceScopeFactory service)
    {
        Schedule(() => new DoctorsMigrationJob(service)).NonReentrant().ToRunNow();
    }
}