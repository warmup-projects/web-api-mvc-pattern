using Microsoft.AspNetCore.Mvc;
using mvc.Models.Sample;
using mvc.Services.Sample.Interface;

namespace mvc.Controllers;

[Route("api/weather-forecast")]
[ApiController]
public class WeatherForecastController : Controller
{
    private readonly IWeatherForecastService _consultationService;
    private readonly IConfiguration _configuration;

    public WeatherForecastController(IWeatherForecastService consultationService, IConfiguration configuration)
    {
        _consultationService = consultationService;
        _configuration = configuration;
    }
    
    [HttpGet("weather-forecast")]
    public ActionResult<WeatherForecast> GetWeatherForecast()
    {
        try
        {
            var summaries = new[]
            {
                "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
            };
            var forecast =  Enumerable.Range(1, 5).Select(index =>
                    new WeatherForecast
                    (
                        DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                        Random.Shared.Next(-20, 55),
                        summaries[Random.Shared.Next(summaries.Length)]
                    ))
                .ToArray();
            return Ok(forecast);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}