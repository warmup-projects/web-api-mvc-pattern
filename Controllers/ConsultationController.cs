using Microsoft.AspNetCore.Mvc;
using mvc.Models;
using mvc.Models.Consultation;
using mvc.Services.Transaction.Interface;

namespace mvc.Controllers;

[Route("api/consultation")]
[ApiController]
public class ConsultationController : Controller
{
    private readonly IConsultationService _consultationService;
    private readonly IConfiguration _configuration;

    public ConsultationController(IConsultationService consultationService, IConfiguration configuration)
    {
        _consultationService = consultationService;
        _configuration = configuration;
    }
    
    [HttpGet("consultation-list")]
    public ActionResult<BaseResponse<List<ConsultationListResponse>>> GetConsultationList([FromQuery] ConsultationListRequest request)
    {
        try
        {
            var result = _consultationService.GetConsultationList(request);
            return Ok(result);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("consultation-decode")]
    public ActionResult<BaseResponse<ConsultationDecodeModel>> ConsultationDecode([FromQuery] ConsultationListRequest request)
    {
        try
        {
            var result = _consultationService.ConsultationDecode(request);
            return Ok(result);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}